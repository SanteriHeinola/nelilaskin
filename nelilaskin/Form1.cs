﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nelilaskin
{
    public partial class Form1 : Form
    {
        // luo tekstilaatikot
        double b1, b2, tulos;
        public DateTime GetStartTime() => Process.GetCurrentProcess().StartTime;
        // public DateTime GetExitTime() => Process.GetCurrentProcess().ExitTime;


        public Form1()
        {
            InitializeComponent();
            Ajat();
        }

        //private void tulostaTiedostoon()
        //{
        //    string tulostaTiedostoon = "C:/temp/nelilaskin.txt";

        //    if (checkBox1.Checked == true)
        //    {
        //        TextWriter tw = new StreamWriter(tulostaTiedostoon, true);
        //        tw.WriteLine(tulos);
        //        tw.Close();
        //    }


        //}

        private void Ajat()
        {
            string Ajat = "C:/temp/nelilaskin.txt";
            Process p = Process.GetCurrentProcess();
            if (p != null)
            {
                TextWriter tw = new StreamWriter(Ajat, true);
                tw.WriteLine("Ohjelman käynnistys: " + p.StartTime);
                tw.Close();
            }
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            b1 = Convert.ToDouble(box1.Text);
            b2 = Convert.ToDouble(box2.Text);
            tulos = b1 - b2;
            textBox3.Text = b1 + " - " + b2 + " = " + tulos.ToString();
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            b1 = Convert.ToDouble(box1.Text);
            b2 = Convert.ToDouble(box2.Text);
            tulos = b1 + b2;
            textBox3.Text = b1 + " + " + b2 + " = " + tulos.ToString();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            b1 = Convert.ToDouble(box1.Text);
            b2 = Convert.ToDouble(box2.Text);
            tulos = b1 * b2;
            textBox3.Text = b1 + " * " + b2 + " = " + tulos.ToString();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            b1 = Convert.ToDouble(box1.Text);
            b2 = Convert.ToDouble(box2.Text);
            tulos = b1 / b2;
            textBox3.Text = b1 + " / " + b2 + " = " + tulos.ToString();

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            box1.Text = String.Empty;
            box2.Text = String.Empty;
            textBox3.Text = String.Empty;
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            string tulostaTiedostoon = "C:/temp/nelilaskin.txt";

            if (checkBox1.Checked == true)
            {
                TextWriter tw = new StreamWriter(tulostaTiedostoon, true);
                tw.WriteLine(tulos);
                tw.Close();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tahdotko sammuttaa?", "Exit", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                string Ajat = "C:/temp/nelilaskin.txt";
                Process p = Process.GetCurrentProcess();
                {
                    TextWriter tw = new StreamWriter(Ajat, true);

                    tw.WriteLine("Ohjelman sulkeminen: " + DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss"));
                    tw.Close();
                }
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {


        }
    }
}
